//
//  ViewController.swift
//  AlgorithmsIOS
//
//  Created by Maria Litvinenko on 25.01.2022.
//

import UIKit

class ViewController: UIViewController {

    // 1 задача
    //    Given a string of digits, you should replace any digit below 5 with '0' and any digit 5 and above with '1'. Return the resulting string.
    //    Note: input will never be an empty string
    
    // Мое решение:
    func fakeBin(digits: String) -> String {
        let initialDigits = digits.map { Int(String($0))! }
        var refactoredDigits = [Int]()
        for i in initialDigits {
            if i < 5 {
                refactoredDigits.append(0)
            } else if i >= 5 {
                refactoredDigits.append(1)
            }
        }
        let stringDigits = refactoredDigits.map { String($0) }
        return stringDigits.joined(separator: "")
    }
    
    // Лучшее найденное решение:
    // Объяснение: Создаем пустую строку. Перебираем все элементы изначальной строки. Конвертируем каждый элемент изначальной строки из String в Int и проверяем, какой он: больше или меньше 5. Если меньше, прибавляем к пустой строке 0, если больше или равен 5 - прибавляем 1. Возвращаем новую строку.
    func bestFakeBin(digits: String) -> String {
        var bin = ""
        for digit in digits {
            if Int("\(digit)")! < 5 {
                bin += "0"
            } else {
                bin += "1"
            }
        }
        return bin
    }
    
    // 2 задача
    // A hero is on his way to the castle to complete his mission. However, he's been told that the castle is surrounded with a couple of powerful dragons! each dragon takes 2 bullets to be defeated, our hero has no idea how many bullets he should carry.. Assuming he's gonna grab a specific given number of bullets and move forward to fight another specific given number of dragons, will he survive?
    //   Return True if yes, False otherwise :)
    
    // Мое решение:
    func hero(bullets: Int, dragons: Int) -> Bool {
        guard bullets >= dragons * 2 else { return false }
        return true
    }
    // Лучшее найденное решение:
    // Объяснение: Если пуль в два раза больше, чем драконов, то возвращаем true, если нет - false
    func bestHero(bullets: Int, dragons: Int) -> Bool {
        return bullets >= dragons * 2
    }
    
    // 3 задача
    // This function should test if the factor is a factor of base.
    // Return true if it is a factor or false if it is not.
    //Factors are numbers you can multiply together to get another number.
    // 2 and 3 are factors of 6 because: 2 * 3 = 6
    
    // Мое решение:
    func checkForFactor(_ base: Int, _ factor: Int) -> Bool {
          return base % factor == 0
      }
    // Решения лучше не нашла
    
    // 4 задача:
    // You will be given an array a and a value x. All you need to do is check whether the provided array contains the value.
    // The type of a and x can be String or Int.
    // Return true if the array contains the value, false if not.
    
    // Мое решение:
    func check<T: Equatable>(_ a: [T], _ x: T) -> Bool {
        return a.contains(x)
    }
    // Решения лучше не нашла
    
    // 5 задача:
    // Your goal is to return multiplication table for number that is always an integer from 1 to 10.
    // For example, a multiplication table (string) for number == 5 looks like below:
    // 1 * 5 = 5
    // 2 * 5 = 10
    // 3 * 5 = 15
    // 4 * 5 = 20
    // 5 * 5 = 25
    // 6 * 5 = 30
    // 7 * 5 = 35
    // 8 * 5 = 40
    // 9 * 5 = 45
    // 10 * 5 = 50
    
    // Мое решение:
    func multi_table(_ number: Int) -> String {
        var string = ""
        for i in 1...9 {
         string += "\(i) * \(number) = \(number*i) \n"
        }
        string += "\(10) * \(number) = \(number*10)"
        return string
    }
    
    // 6 задача:
    // Write function bmi that calculates body mass index (bmi = weight / height2).
    // if bmi <= 18.5 return "Underweight"
    // if bmi <= 25.0 return "Normal"
    // if bmi <= 30.0 return "Overweight"
    // if bmi > 30 return "Obese"
    
    // Мое решение:
    func bmi(_ weight: Int, _ height: Double) -> String {
        let bmi = Double(weight) / (height * height)
        let result = ""
        if bmi <= 18.5 {
            return "Underweight"
        } else if bmi <= 25.0 {
            return "Normal"
        } else if bmi <= 30.0 {
            return "Overweight"
        } else if bmi > 30.0 {
            return "Obese"
        }
        return result
    }
    // Лучшее найденное решение:
    // Объяснение: Присваиваем результат формулы переменной. Проверяем размер переменной и возвращаем соответствующее значение.
    func bestBmi(_ weight: Int, _ height: Double) -> String {
        let bmi = Double(weight)/(height * height)
        switch bmi {
          case 0...18.5:
            return "Underweight"
          case 18.5...25.0:
            return "Normal"
          case 25.0...30.0:
            return "Overweight"
          default:
            return "Obese"
        }
    }
    
    // 7 задача:
    // Write a function that takes in a string of one or more words, and returns the same string, but with all five or more letter words reversed (Just like the name of this Kata). Strings passed in will consist of only letters and spaces. Spaces will be included only when more than one word is present.
    // Examples: spinWords( "Hey fellow warriors" ) => returns "Hey wollef sroirraw" spinWords( "This is a test") => returns "This is a test" spinWords( "This is another test" )=> returns "This is rehtona test"
    // Мое решение:
    func spinWords(in sentence: String) -> String {
        let arrayOfWords = sentence.components(separatedBy: " ")
        var arrayOfReversedWords = [String]()
        for i in arrayOfWords {
            if i.count >= 5 {
                arrayOfReversedWords.append(String(i.reversed()))
            } else {
                arrayOfReversedWords.append(i)
            }
        }
        return arrayOfReversedWords.joined(separator: " ")
    }
    // Лучшее найденное решение:
    // Объяснение: Разбиваем строку на отдельные строки, если между словами есть пробелы. Пробегаемся по каждой отдельной строке. Если кол-во символов в строке больше четырех, то меняем символы местами, если нет - оставляем как есть. Соединяем все получившиеся строки в одну.
    func bestSpinWords(in sentence: String) -> String {
        sentence
            .split(separator: " ")
            .map { "\($0)" }
            .map { $0.count > 4 ? String($0.reversed()) : $0 }
            .joined(separator: " ")
    }
    
    // 8 задача:
    // Given an array of integers, return a new array with each value doubled.
    // For example:
    // [1, 2, 3] --> [2, 4, 6]
    
    // Мое решение:
    func maps(a : Array<Int>) -> Array<Int> {
        a.map { $0 * 2 }
    }
    // Решения лучше не нашла
    
    // 9 задача:
    // Given a non-empty array of integers, return the result of multiplying the values together in order. Example: [1, 2, 3, 4] => 1 * 2 * 3 * 4 = 24
    
    // Мое решение:
    func grow(_ arr: [Int]) -> Int {
        arr.reduce(1, { $0 * $1 })
    }
    // Лучшее найденное решение:
    // Объяснение: Проходимся по массиву и перемножаем все элементы.
    func bestGrow(_ arr: [Int]) -> Int {
      return arr.reduce(1,*)
    }
    
    // 10 задача:
    // You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.
    
    // Мое решение:
    func findOutlier(_ array: [Int]) -> Int {
        var evenNumbers = [Int]()
        var oddNumbers = [Int]()
        for i in array {
            if i % 2 == 0 {
                evenNumbers.append(i)
            } else {
                oddNumbers.append(i)
            }
        }
        if oddNumbers.count > evenNumbers.count {
            return evenNumbers[0]
        } else {
            return oddNumbers[0]
        }
    }
    // Лучшее найденное решение:
    // Объяснение: Создаем два массива: с четными и нечетными числами. Проверяем, где элементов больше: если в первом, то возвращаем первый и единственный элемент второго, и наоборот.
    func bestFindOutlier(_ array: [Int]) -> Int {
      let evens = array.filter { $0 % 2 == 0 }
      let odds = array.filter { $0 % 2 != 0 }
      return evens.count < odds.count ? evens[0] : odds[0]
    }
    
    // 11 задача:
    // Clock shows h hours, m minutes and s seconds after midnight. Your task is to write a function which returns the time since midnight in milliseconds.
    // h = 0
    // m = 1
    // s = 1
    // result = 61000
    
    // Мое решение:
    func past(_ h: Int, _ m: Int, _ s: Int) -> Int {
        h * 3600000 + m * 60000 + s * 1000
    }
    // Решения лучше не нашла
    
    // 12 задача:
    // Create a function that takes an integer as an argument and returns "Even" for even numbers or "Odd" for odd numbers.
    
    // Мое решение:
    func evenOrOdd(_ number:Int) -> String {
        number % 2 == 0 ? "Even" : "Odd"
    }
    // Решения лучше не нашла
    
    // 13 задача:
    // Implement a function which convert the given boolean value into its string representation.
    
    // Мое решение:
    func booleanToString(_ b: Bool) -> String {
        b == true ? "true" : "false"
    }
    
    // Лучшее найденное решение:
    // Объяснение: Конвертируем bool в string
    func bestBooleanToString(_ b: Bool) -> String {
      return String(b)
    }
    
    // 14 задача:
    // Given a random non-negative number, you have to return the digits of this number within an array in reverse order.
    // Example:
    // 48597 => [7,9,5,8,4,3]
    // 0 => [0]
    // Мое решение:
    func digitize(_ num:Int) -> [Int] {
        String(num).compactMap{ $0.wholeNumberValue }.reversed()
    }
    // Решения лучше не нашла
    
    // 14 задача:
    // Given a random non-negative number, you have to return the digits of this number within an array in reverse order.
    // Example:
    // 48597 => [7,9,5,8,4,3]
    // 0 => [0]
    func bestDigitize(_ num:Int) -> [Int] {
        String(num).compactMap{ $0.wholeNumberValue }.reversed()
    }
    // Решения лучше не нашла
    
    // Write a simple camelCase function for strings. All words must have their first letter capitalized and all spaces removed.
    // For instance: camelCase("hello case"); // ==> "HelloCase"

    // Мое решение:
    func camelCase(_ str: String) -> String {
        return str.split(separator: " ").map { $0.capitalized }.joined()
    }
    
    // Лучшее найденное решение:
    // Объяснение: Капитализируем все слова и вместо пробелов объединяем их.
    func bestCamelCase(_ str: String) -> String {
      return str.capitalized.replacingOccurrences(of: " ", with: "")
    }
    
    // 15 задача:
    // Write a function called repeatStr which repeats the given string string exactly n times.
    // repeatStr(6, "I") // "IIIIII"
    // repeatStr(5, "Hello") // "HelloHelloHelloHelloHello"

    // Мое решение:
    func repeatStr(_ n: Int, _ string: String) -> String {
        return String(repeating: string, count: n)
    }
    
    // 16 задача:
    // Create a function that checks if a number n is divisible by two numbers x AND y. All inputs are positive, non-zero digits.
    // Мое решение:
    func isDivisible(_ n: Int, _ x: Int, _ y: Int) -> Bool {
        n % x == 0 && n % y == 0 ? true : false
    }
    
    // Лучшее найденное решение:
    // Объяснение: Если число делится на оба числа без остатка, возвращаем true
    func bestIsDivisible(_ n: Int, _ x: Int, _ y: Int) -> Bool {
        return n % x == 0 && n % y == 0
    }
    
    // 17 задача:
    //Some new cashiers started to work at your restaurant. They are good at taking orders, but they don't know how to capitalize words, or use a space bar! All the orders they create look something like this:
    // "milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza"
    // The kitchen staff are threatening to quit, because of how difficult it is to read the orders. Their preference is to get the orders as a nice clean string with spaces and capitals like so:
    // "Burger Fries Chicken Pizza Pizza Pizza Sandwich Milkshake Milkshake Coke"
    
    // Мое решение:
    func getOrder(_ input: String) -> String {
        let menuItems = ["burger", "fries", "chicken", "pizza", "sandwich", "onionrings", "milkshake", "coke"]
        var sortedItems = [String]()
        for menuItem in menuItems {
            if input.contains(menuItem) {
                sortedItems.append(menuItem)
            }
        }
        return sortedItems.joined(separator: " ").capitalized
    }
    // Решения лучше не нашла
    
    // 18 задача:
    // Digital root is the recursive sum of all the digits in a number.
    // Given n, take the sum of the digits of n. If that value has more than one digit, continue reducing in this way until a single-digit number is produced. The input will be a non-negative integer.
    
    // Мое решение:
    func digitalRoot(of number: Int) -> Int {
        let root = String(number).compactMap({$0.wholeNumberValue}).reduce(0, +)
        if String(root).count > 1 {
            return digitalRoot(of: root)
        } else {
            return root
        }
    }
    // Лучшее найденное решение:
    // Объяснение: 9 * n % 9 = 0
    func bestDigitalRoot(of number: Int) -> Int {
        return (1 + (number - 1) % 9)
    }
    
    // 19 задача:
    // Is similar to factorial of a number, In primorial, not all the natural numbers get multiplied, only prime numbers are multiplied to calculate the primorial of a number. It's denoted with P# and it is the product of the first n prime numbers.
    
    // Мое решение:
    func numPrimoral( _ number: UInt ) -> UInt {
        let intArray: [Int] = Array(2...1000000)
        var sortedIntArray = intArray.filter{ $0 % 2 == 1 }
        sortedIntArray.insert(2, at: 0)
        let arrayOfWhatWeNeed = Array(sortedIntArray.prefix(Int(number))).reduce(1, *)
        return UInt(Int(integerLiteral: arrayOfWhatWeNeed))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print(numPrimoral(5))
    }
    
}

